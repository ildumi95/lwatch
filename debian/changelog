lwatch (0.6.2-2) unstable; urgency=medium

  * QA upload
  * debian/rules: package now builds with RISC-V using dh $@
  * debian/control: updated dependencies and removed some lintian issues
  * debian/copyright: updated to format 1.0
  * debian/compat: deleted to remove lintian issue
  * debian/watch: updated version to 4

 -- Debian QA Group <packages@qa.debian.org>  Thu, 16 Dec 2021 15:15:18 +0100

lwatch (0.6.2-1) unstable; urgency=medium

  * New upstream release
    + Updated config.{guess,sub} files (Closes: #727927)
  * Add lwatch-dbg package
  * Remove DM-Upload-Allowed from debian/control
  * Added linker/compiler options for hardening in debian/rules
  * Update Standards-Version to 3.9.5 - no changes required

 -- Artur R. Czechowski <arturcz@hell.pl>  Sat, 11 Jan 2014 17:29:03 +0100

lwatch (0.6-1) unstable; urgency=low

  * New upstream release
  * Add pkg-config to build-depends
  * Remove removing fifo command from debian/rules, the upstream no more
    creates the file during make install
  * Install empty /var/lib/lwatch directory
  * Convert to 3.0 (quilt) format
  * debian/control: fixed long and short description as suggested by lintian
  * debian/postinst: replace mknod with mkfifo, remove mknod lintian override
  * Proper handling of all action in maintainer scripts
  * debian/rules: add targets: build-arch and build-indep
  * Update debian/watch file to track .bz2 extension instead of .gz one.
  * Update Standards-Version to 3.9.3 - no changes required

 -- Artur R. Czechowski <arturcz@hell.pl>  Sat, 24 Mar 2012 14:16:35 +0100

lwatch (0.5-1+cvs20100211) experimental; urgency=low

  * Current CVS snapshot - it shall fix the hurd build problems
  * Added flex to build depends

 -- Artur R. Czechowski <arturcz@hell.pl>  Thu, 11 Feb 2010 18:45:08 +0000

lwatch (0.5-1) unstable; urgency=low

  * New upstream release
    + autotools stuff is updated (Closes: #533692)
  * debian/watch uses sf.net redirector
  * Update Standards-Version yo 3.8.3 (no changes required)
  * Use debhelper 7
  * debian/rules: replace dh_clean -k with dh_prep

 -- Artur R. Czechowski <arturcz@hell.pl>  Sat, 28 Nov 2009 16:54:48 +0000

lwatch (0.4.1-2) unstable; urgency=low

  * don't remove yparse.c on clean (Closes: #424562), the same change
    has been made in upstream's CVS
  * lwatch's distclean target is run via [ ! -f Makefile ] || $(MAKE) distclean
    instead of -$(MAKE) distclean to cool out lintian
  * Bump up the Standards-Version (nothing to change)
  * lintian override: mknod-in-maintainer-script, FIFO is created, not device
  * update debian/compat from 4 to 5, Build-Depends on debhelper (>= 5.0.0)

 -- Artur R. Czechowski <arturcz@hell.pl>  Sat, 07 Jul 2007 21:08:59 +0200

lwatch (0.4.1-1) unstable; urgency=low

  * Initial Debian Release (Closes: #187282)

 -- Artur R. Czechowski <arturcz@hell.pl>  Sat, 20 Aug 2005 20:14:55 +0200
