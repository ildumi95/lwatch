/* die.h -- LogWatcher

   This file is part of the LogWatcher tool.

   Copyright (C) 2002-2009 Artur Robert Czechowski

   The LogWatcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.

   The LogWatcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the LogWatcher; see the file COPYING.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Artur R. Czechowski
   <arturcz@hell.pl>
   http://hell.pl/arturcz/
 */

#ifndef _ARC_CONTROL_H
#define _ARC_CONTROL_H

#include <errno.h>
#include <signal.h>
#include <string.h>

#include <sys/poll.h>

#define io_check(res,str) if((res)==-1) die("%s%s[%i] %s\n",str,strlen(str)?": ":"",errno,strerror(errno))
#define io_fcheck(res,str) if((res)==NULL) die("%s%s[%i] %s\n",str,strlen(str)?": ":"",errno,strerror(errno))
#define sig_check(res,str) if((res)==SIG_ERR) die("%s%sCannot set signal handler\n",str,strlen(str)?": ":"");

void die(const char *s, ...);
void signal_handle(int i);
void set_handlers(void);
#ifdef DEBUG
void show_poll_res(int res, struct pollfd *ufds, int n);
#endif

extern int loop;

#endif
