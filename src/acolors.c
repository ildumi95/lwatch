/* acolors.c -- LogWatcher

   This file is part of the LogWatcher tool.

   Copyright (C) 2002-2009 Artur Robert Czechowski

   The LogWatcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.

   The LogWatcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the LogWatcher; see the file COPYING.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Artur R. Czechowski
   <arturcz@hell.pl>
   http://hell.pl/arturcz/
 */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "acolors.h"
#include "config.h"
#include "data.h"


const char *colmode[] = {
    "",
    CLEAR,
    BOLD,
    REVERSE,
    CONCEALED,
    BLACK,
    RED,
    GREEN,
    BROWN,
    BLUE,
    MAGENTA,
    CYAN,
    LIGHTGRAY,
    DARKGRAY,
    BRIGHTRED,
    BRIGHTGREEN,
    YELLOW,
    BRIGHTBLUE,
    PURPLE,
    BRIGHTCYAN,
    WHITE
};

const struct s_str_to_val colstr[] = {
    {"clear", _N_CLEAR},
    {"reset", _N_RESET},
    {"bold", _N_BOLD},
    {"reverse", _N_REVERSE},
    {"concealed", _N_CONCEALED},
    {"black", _N_BLACK},
    {"red", _N_RED},
    {"green", _N_GREEN},
    {"brown", _N_BROWN},
    {"blue", _N_BLUE},
    {"magenta", _N_MAGENTA},
    {"cyan", _N_CYAN},
    {"lightgray", _N_LIGHTGRAY},

    {"darkgray", _N_DARKGRAY},
    {"brightred", _N_BRIGHTRED},
    {"brightgreen", _N_BRIGHTGREEN},
    {"yellow", _N_YELLOW},
    {"brightblue", _N_BRIGHTBLUE},
    {"purple", _N_PURPLE},
    {"brightcyan", _N_BRIGHTCYAN},
    {"white", _N_WHITE},
    {NULL, -1}
};

/*
 * Thanks to Andrzej Oszer <oszer@poczta.onet.pl> for idea to those functions.
 * This idea have been taken from LIB - http://sf.net/projects/lib
 */

#define CP_ALLOC 16
#define MAX_COL_STACK 20

int ascprintf(char **pstr, char *format) {
    int col_stack[MAX_COL_STACK];
    int col_stack_ptr = 0;
    int allocated = 0;
    int len = 0;
    char *outstr;
    char *src;
    char numer[3];
    int val;
    outstr = (char *)malloc(CP_ALLOC);
    allocated = CP_ALLOC;
    src = format;
    while (*src) {
        if (*src == '^') {
            if (((src[1] >= '0') && (src[1] <= '9')) &&
                    ((src[2] >= '0') && (src[2] <= '9'))) {
                const char *color;
                src++;
                numer[0] = *src;
                src++;
                numer[1] = *src;
                src++;
                numer[2] = '\0';
                val = atoi(numer);
                if (val < 0) {
                    val = 0;
                }
                if (val) {
                    col_stack[++col_stack_ptr] = val;
                } else {
                    val = col_stack[--col_stack_ptr];
                }
                color = colmode[val];
                while (*color) {
                    outstr[len++] = *color++;
                    if (len == allocated) {
                        allocated += CP_ALLOC;
                        outstr = (char *)realloc((void *)outstr, allocated);
                    }
                }
                continue;
            }
        }
        outstr[len++] = *src++;
        if (len == allocated) {
            allocated += CP_ALLOC;
            outstr = (char *)realloc((void *)outstr, allocated);
        }
    }
    outstr[len] = '\0';
    *pstr = outstr;
    return(len - 1);
}

int cprintf(char *format) {
    char *str;
    int i;
    i = ascprintf(&str, format);
    printf("%s", str);
    free(str);
    return(i);
}
