/* settings.h -- LogWatcher

   This file is part of the LogWatcher tool.

   Copyright (C) 2002-2009 Artur Robert Czechowski

   The LogWatcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.

   The LogWatcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the LogWatcher; see the file COPYING.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Artur R. Czechowski
   <arturcz@hell.pl>
   http://hell.pl/arturcz/
 */

#ifndef _ARC_SETTINGS_H
#define _ARC_SETTINGS_H

#include <pcre.h>

#include "defaults.h"

struct s_conf {
    int def_date_color, def_host_color, def_serv_color, def_mesg_color;
    char *conf_file;
    char *in_file;
    char *out_file;
    int show_unparsed;
    int daemon_mode;
    int use_syslog;
    int log_level;
    int def_action_rule;
    int cfg_ver; /* wersja formatu pliku konfiguracyjnego */
};


struct s_action {
#ifdef DEBUG
    char *restr;
#endif
    pcre *re;
    pcre_extra *rh;
    int date_color,
        host_color,
        serv_color,
        mesg_color,
        highlight_color;	/* highlight color from re */
    int match_service,	/* match service instead message */
        match_host,		/* match hostname instead message */
        ignore,		/* ignore this line and do not display */
        action;		/* process next rule? */
};

void parse_options(int argc, char **argv);
void parse_config(void);
void add_action(struct s_action *paction);
void free_settings(void);

extern struct s_conf lw_conf;
extern struct s_action *lw_actions;
extern int no_actions;

#define action_exit(a) ((a).action==RULE_EXIT)||(((a).action==RULE_DEFAULT)&&(lw_conf.def_action_rule==RULE_EXIT))

#endif
