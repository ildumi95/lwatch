/* log.c - syslog support, output errors */
/*
 *  This file is part of the LogWatcher tool.
 *  Copyright (C) 2003,2010  Artur R. Czechowski <arturcz@hell.pl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "log.h"
#include "settings.h"

#define quote_value_pair(v) #v,v
const struct s_str_to_val loglevelstr[] = {
    { quote_value_pair(LOG_EMERG) },
    { quote_value_pair(LOG_ALERT) },
    { quote_value_pair(LOG_CRIT) },
    { quote_value_pair(LOG_ERR) },
    { quote_value_pair(LOG_WARNING) },
    { quote_value_pair(LOG_NOTICE) },
    { quote_value_pair(LOG_INFO) },
    { quote_value_pair(LOG_DEBUG) },
    { NULL, -1 }
};
#undef quote_value_pair

int logger_subsystem_initialized = 0;

void start_log(void) {
    openlog("lwatch", LOG_CONS | LOG_PID, LOG_DAEMON);
    logger_subsystem_initialized = 1;
}

void stop_log(void) {
    closelog();
    logger_subsystem_initialized = 0;
}

void vsend_log(int prio, const char *fmt, va_list ap) {
    if (!logger_subsystem_initialized) {
        vfprintf(stderr, fmt, ap);
        fprintf(stderr, "send_log: Fatal error. Logger subsystem uninitialized. Please send a bugreport.\n");
        exit(1);
    }
    if (prio <= lw_conf.log_level) {
        vsyslog(prio, fmt, ap);
    }
}

void send_log(int prio, const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    vsend_log(prio, fmt, ap);
    va_end(ap);
}
