/* settings.c -- LogWatcher

   This file is part of the LogWatcher tool.

   Copyright (C) 2002-2009 Artur Robert Czechowski

   The LogWatcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.

   The LogWatcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the LogWatcher; see the file COPYING.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Artur R. Czechowski
   <arturcz@hell.pl>
   http://hell.pl/arturcz/
 */

#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include "config.h"
#include "acolors.h"
#include "defaults.h"
#include "control.h"
#include "settings.h"
#include "strpcre.h"
#include "yparse.h"

struct s_conf lw_conf;
struct s_action *lw_actions;
int no_actions;
int omit_config = 0;

void show_configure_options() {
    printf("\nCompiled with following ./configure options:\n" ALLPARAMS "\n");
}

void show_licence() {
    int i;
    char *usage[] = {
        "Log Watcher, the log colourize tool, version " VERSION,
        "Copyright (C) 2002-2009 by Artur R. Czechowski <arturcz@hell.pl>",
        "http://sourceforge.net/projects/lwatch",
        "",
        "This program is free software; you can redistribute it and/or modify",
        "it under the terms of the GNU General Public License as published by",
        "the Free Software Foundation; either version 2 of the License, or",
        "(at your option) any later version.",
        "",
        "This program is distributed in the hope that it will be useful,",
        "but WITHOUT ANY WARRANTY; without even the implied warranty of",
        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the",
        "GNU General Public License for more details.",

        "",
        "You should have received a copy of the GNU General Public License",
        "along with this program; if not, write to the Free Software",
        "Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA",
        "",
        "Regular expression support is provided by the PCRE library package,",
        "which is open source software, written by Philip Hazel, and copyright",
        "by the University of Cambridge, England.",
        "PCRE library available at:",
        "ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/",
        NULL
    };
    i = 0;
    while (usage[i]) {
        printf("%s\n", usage[i++]);
    }
    show_configure_options();
    exit(0);
}

void show_usage() {
    int i;
    char *usage[] = {
        "Options:",
        "  -v, --version",
        "\tshow version, copyright notices and ./configure options",
        "  -C, --config filename",
        "\tread config from filename instead of " DEF_CONF_FILE ",",
        "  -i, --input filename",
        "\tread data from named fifo filename instead of " DEF_IN_FILE ",",
        "  -o, --output filename",
        "\twrite colored logs into filename instead of " DEF_OUT_FILE ",",
        "  -s, --show-unparsed",
        "\tshow unparsed lines (like `last message repeated X times')",
        "  -d, --daemon",
        "\trun as daemon (move to background and detach from control terminal",
        "  -O, --omit-rc",
        "\tdo not read values from config file",
        "  -h, --help",
        "\tshow this info",
        NULL
    };
    i = 0;
    while (usage[i]) {
        printf("%s\n", usage[i++]);
    }
    exit(0);
}

void parse_options(int argc, char **argv) {
    static struct option opt[] = {
        {"version", 0, 0, 'v'},
        {"config", 1, 0, 'C'},
        {"input", 1, 0, 'i'},
        {"output", 1, 0, 'o'},
        {"show-unparsed", 0, 0, 's'},
        {"daemon", 0, 0, 'd'},
        {"omit-rc", 0, 0, 'O'},
        {"help", 0, 0, 'h'},
        {0, 0, 0, 0}
    };
    int idx, c;
    /* char *pc; //what I needed it for? */

    /* initialize some stuff */
    lw_actions = NULL;
    no_actions = 0;
#ifdef DEBUG
    fprintf(stderr, "Parse options\n");
#endif
    memset(&lw_conf, 0, sizeof(lw_conf));
    lw_conf.use_syslog = 1;
    lw_conf.log_level = LOG_NOTICE;
    while ((c = getopt_long(argc, argv, "vC:f:i:o:Osdht", opt, &idx)) != EOF) {
        switch (c) {
            case 'C':
                lw_conf.conf_file = strdup(optarg);
#ifdef DEBUG
                fprintf(stderr, "Configuration file set to: %s\n", lw_conf.conf_file);
#endif
                break;
            case 'i':
                if (lw_conf.in_file) {
                    free(lw_conf.in_file);
                }
                lw_conf.in_file = strdup(optarg);
#ifdef DEBUG
                fprintf(stderr, "Input file set to: %s\n", lw_conf.in_file);
#endif
                break;
            case 'o':
                if (lw_conf.out_file) {
                    free(lw_conf.out_file);
                }
                lw_conf.out_file = strdup(optarg);
#ifdef DEBUG
                fprintf(stderr, "Output file set to: %s\n", lw_conf.out_file);
#endif
                break;
            case 's':
                lw_conf.show_unparsed = 1;
#ifdef DEBUG
                fprintf(stderr, "Shows unparsed lines\n");
#endif
                break;
            case 'd':
                lw_conf.daemon_mode = 1;
#ifdef DEBUG
                fprintf(stderr, "Run in daemon mode\n");
#endif
                break;
            case 'O':
                omit_config = 1;
#ifdef DEBUG
                fprintf(stderr, "Omit config file\n");
#endif
                break;
            case 'v':
                show_licence();
                break;
            case 'h':
                show_usage();
                break;
            case 0:
                printf("Long option not supported now\n");
                break;
            default:
                printf("Type %s -h for help\n", argv[0]);
                exit(1);
        }
    }
}

void parse_config(void) {
    lw_conf.def_date_color = _N_CYAN;
    lw_conf.def_host_color = _N_MAGENTA;
    lw_conf.def_serv_color = _N_BLUE;
    lw_conf.def_mesg_color = _N_LIGHTGRAY;
    lw_conf.def_action_rule = DEF_ACTION_RULE;
    if (omit_config) {
        return;
    }
    if (!lw_conf.conf_file) {
        lw_conf.conf_file = strdup(DEF_CONF_FILE);
    }
#ifdef DEBUG
    fprintf(stderr, "Open config file: %s\n", lw_conf.conf_file);
#endif
    io_fcheck(yyin = fopen(lw_conf.conf_file, "r"), lw_conf.conf_file);
#ifdef DEBUG
    fprintf(stderr, "Parse...\n");
#endif
    yylex();
#ifdef DEBUG
    fprintf(stderr, "Finished\n");
#endif
    fclose(yyin);
    /* Tymczasowo przeniesione do lwatch.c *
    if(!strlen(lw_conf.in_file))
    	strncpy(lw_conf.in_file,DEF_IN_FILE,MAXPATHLEN);
    if(!strlen(lw_conf.out_file))
    	strncpy(lw_conf.out_file,DEF_OUT_FILE,MAXPATHLEN);
     ***************************************/
#ifdef DEBUG
    fprintf(stderr, "date: %i, host: %i, serv: %i, mesg: %i, default rule action: %s\n",
            lw_conf.def_date_color,
            lw_conf.def_host_color,
            lw_conf.def_serv_color,
            lw_conf.def_mesg_color,
            (lw_conf.def_action_rule == RULE_EXIT) ? "exit" : "continue"
           );
#endif

    if (!lw_conf.cfg_ver) die("\nERROR! Not versioned configuration file!\n"
                                  "Read about cfg_ver in lwatch.conf(5).\n\n");
}

void add_action(struct s_action *paction) {
    lw_actions = (struct s_action *)realloc(lw_actions,
                                            sizeof(struct s_action) * (no_actions + 1));
    if (lw_actions == NULL) {
        die("Cannot reallocate %i bytes of memory\n",
            sizeof(struct s_action) * (no_actions + 1));
    }
    memcpy(&lw_actions[no_actions], paction, sizeof(struct s_action));
#ifdef DEBUG
    fprintf(stderr, "vvvvvvvvvvvvvvvv\n");
    fprintf(stderr, "RE: %s\ndate: [%i]\nhost: [%i]\nserv: [%i]\nmesg: [%i]\n",
            paction->restr,
            paction->date_color,
            paction->host_color,
            paction->serv_color,
            paction->mesg_color);
    fprintf(stderr, "Highlight: %i\nIgnore: %s\nExit: %s\nMatch service: %s\nMatch host: %s\n",
            paction->highlight_color,
            paction->ignore ? "yes" : "no",
            (paction->action == RULE_DEFAULT) ? (action_exit(*paction) ? "yes (default)" : "no (default)") : (action_exit(*paction) ? "yes" : "no"),
                paction->match_service ? "yes" : "no",
                paction->match_host ? "yes" : "no");
    fprintf(stderr, "^^^^^^^^^^^^^^^^\n");
#endif
    no_actions++;
}

void free_settings(void) {
    while (no_actions--) {
#ifdef DEBUG
        fprintf(stderr, "Freed resources for action [%i]%s\n",
                no_actions, lw_actions[no_actions].restr);
#endif
        free_re(&lw_actions[no_actions].re, &lw_actions[no_actions].rh);
#ifdef DEBUG
        freestr(lw_actions[no_actions].restr);
#endif
    }
    free(lw_actions);
#ifdef DEBUG
    fprintf(stderr, "Freed memory for actions\n");
#endif
    if (lw_conf.in_file) {
        free(lw_conf.in_file);
    }
    if (lw_conf.out_file) {
        free(lw_conf.out_file);
    }
    if (lw_conf.conf_file) {
        free(lw_conf.conf_file);
    }
}
