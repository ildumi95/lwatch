/* defaults.h -- LogWatcher

   This file is part of the LogWatcher tool.

   Copyright (C) 2002-2009 Artur Robert Czechowski

   The LogWatcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.

   The LogWatcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the LogWatcher; see the file COPYING.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Artur R. Czechowski
   <arturcz@hell.pl>
   http://hell.pl/arturcz/
 */

#ifndef _ARC_DEFAULT_H
#define _ARC_DEFAULT_H

#include "config.h"

#define YES 1
#define NO 0

#define RULE_DEFAULT 0
#define RULE_EXIT 1
#define RULE_CONTINUE 2

#define DEF_DATE_COLOR CYAN
#define DEF_HOST_COLOR MAGENTA
#define DEF_SERV_COLOR BLUE
#define DEF_MESG_COLOR LIGHTGRAY
#define DEF_CONF_FILE SYSCONFDIR "/lwatch.conf"
#define DEF_IN_FILE INPUTFILE
#define DEF_OUT_FILE "-"
#define DEF_CREATE_FIFO YES
#define DEF_ACTION_RULE RULE_CONTINUE

#endif
