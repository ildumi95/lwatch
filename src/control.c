/* control.c -- LogWatcher

   This file is part of the LogWatcher tool.

   Copyright (C) 2002-2009 Artur Robert Czechowski

   The LogWatcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.

   The LogWatcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the LogWatcher; see the file COPYING.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Artur R. Czechowski
   <arturcz@hell.pl>
   http://hell.pl/arturcz/
 */

#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/poll.h>

#include "config.h"
#include "control.h"
#include "log.h"

int loop;

void die(const char *s, ...) {
    va_list ap;
#ifdef DEBUG
    va_start(ap, s);
    vfprintf(stderr, s, ap);
    va_end(ap);
#endif
    va_start(ap, s);
    vsend_log(LOG_ERR, s, ap);
    va_end(ap);
    exit(1);
}

void signal_handle(int i) {
    int ex;
    ex = 0;
    send_log(LOG_NOTICE, "Got signal %d", i);
    switch (i) {
        case SIGHUP:   /*  1 */
        case SIGINT:   /*  2 */
        case SIGQUIT:  /*  3 */
        case SIGTERM:  /* 15 */
            loop = 0;
            break;
        case SIGSEGV:
            ex = 1;
            loop = 0;
            break;
    }
#ifdef DEBUG
    fprintf(stderr, "Received signal %i%s\n", i, loop ? "" : " - exiting");
#endif
    if (ex) {
        die("Received signal %i%s\n", i, loop ? "" : " - exiting");
    }
}

void set_handlers(void) {
    int i;
    for (i = 1; i < SIGSYS; i++) {
        if ((i == SIGKILL) || (i == SIGSTOP) || (i == SIGSEGV)) {
            continue;
        }
        sig_check(signal(i, &signal_handle), "");
    }
}

#ifdef DEBUG
void show_poll_res(int res, struct pollfd *ufds, int n) {
    int i;
    i = 0;
    while (i < n) {
        fprintf(stderr, "fd: %i res: %i\n", ufds[i].fd, res);
        fprintf(stderr, "watch: %i\n", ufds[i].events);
        fprintf(stderr, "return: %i\n", ufds[i++].revents);
    }
}
#endif
