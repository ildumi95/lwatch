Name:		lwatch
Version:	0.5
Release:	1%{?dist}
Summary:	Colourizing a system logs for easier reading.

License:	GPL
URL:		http://sourceforge.net/projects/lwatch
Source0:	http://downloads.sourceforge.net/project/lwatch/lwatch/0.5/lwatch-0.5.tar.gz
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	sed flex docbook-utils pcre-devel
Requires:	pcre

%description
Colourizing a system logs for easier reading.

%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING INSTALL NEWS README TODO ChangeLog
%{_bindir}/*
%{_mandir}/man?/%{name}*
%config(noreplace) %{_sysconfdir}/lwatch.conf

%changelog
* Thu Feb 04 2010 Soeren grunewald <soeren.grunewald@gmx.net> - 0.5-1
- Initial build
